import java.util.concurrent.atomic.AtomicInteger;

public class TestClass {
    AtomicInteger t = new AtomicInteger(5);
    @Threads(count = 3, total = 5)
    public void testMethod1(){
        System.out.println("я работаю");
    }

    @Threads(count = 2, total = 3)
    public void testMethod2(){
        System.out.println("я тоже");
    }

    @Threads(count = 5)
    public void testMethod3(){
        System.out.println("и я");
    }

}
