import java.lang.reflect.Method;
import java.util.concurrent.atomic.AtomicInteger;

public class Checker {
    private final AtomicInteger total;

    public Checker(int total) {
        this.total = new AtomicInteger(total);
    }

    public boolean check(){
        return total.decrementAndGet() >= 0;
    }
}
