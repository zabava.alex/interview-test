import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class Runner {
    private static final HashMap<Method, Checker> checkerHashMap = new HashMap<>();
    public static void run(Object current){
        ExecutorService executorService = Executors.newCachedThreadPool();
        for(Method method : current.getClass().getDeclaredMethods()){
            if(method.isAnnotationPresent(Threads.class)) {
                int count = method.getAnnotation(Threads.class).count();
                int total = method.getAnnotation(Threads.class).total();
                if (count != 0) {
//                    ExecutorService executorService = Executors.newFixedThreadPool(count);
                    Checker checker = new Checker(total);
                    for (int i = 0; i < count; i++) {
                        if (total == 0) {
                            executorService.submit(() -> method.invoke(current));
                        } else {
                            executorService.submit(() -> {
                                try {
                                    while (checker.check()) {
                                        method.invoke(current);
                                    }
                                } catch (IllegalAccessException | InvocationTargetException e) {
                                    e.printStackTrace();
                                }
                            });
                        }
                    }
                }
            }
        }
        executorService.shutdown();
    }
}
